/*
 * Eric Minaker
 * CSCI371
 * Assignment 1
 * The following will read a file called source.data and intrepret it one line
 * at a time, reading each line into an ArrayList. It will then read each number
 * of that ArrayList, placing it into a new ArrayList called numbers. It will
 * then iterate through that ArrayList, interpreting each number and performing
 * ther approriate action for the number. These actions may be found within the
 * switch statement beginning at line 39. Each case explains the functions that
 * the particular number will perform.
 */
import java.util.*;
import java.io.*;

public class microParse{
    public static ArrayList<String> inputFile = new ArrayList<String>();
    public static ArrayList<String> numbers = new ArrayList<String>();
    public static Stack<Integer> stack = new Stack<Integer>();

    public void read(String filename) throws FileNotFoundException{
        // Reads the given file and populates the inputFile ArrayList with the
        // lines of the file.
        Scanner fileIn = new Scanner(new FileReader(filename));
        while(fileIn.hasNextLine()){
            String line = fileIn.nextLine();
            inputFile.add(line);
        }
    }

    public boolean parse(){
        // Iterates through each number of a line, stored in the 'numbers'
        // ArrayList, and evaluates the validity of the line according to the
        // commands represented by each number. The commands are detailed
        // further within each case. A line is valid if the line passes each
        // case and the first number remaining on the stack is 1.
        Boolean valid = true;
        int a, b, i, parseNum;
        for (Iterator<String> iterator = numbers.iterator();
            iterator.hasNext();) {
            String number = iterator.next();
            try{
                parseNum = Integer.parseInt(number);
                if(parseNum > 9 || parseNum < 0)
                    valid = false;
                try{
                    switch(parseNum){
                        case 0:
                            // Pops two numbers, adds them, and pushes the
                            // result to the stack. Case is valid as long as two
                            // integers are available to pop. If not,
                            // EmptyStackException is thrown.
                            a = stack.pop();
                            b = stack.pop();
                            stack.push(a + b);
                            break;
                        case 1:
                            // Pops two numbers, subtracts the first number
                            // popped from the second and then pushes the result
                            // to the stack. Case is valid as long as two
                            // integers are avaialable to pop. If not,
                            // EmptyStackException is thrown.
                            a = stack.pop();
                            b = stack.pop();
                            stack.push(b - a);
                            break;
                        case 2:
                            // Pops two numbers, multiplies them, and pushes the
                            // result to the stack. Case is valid as long as two
                            // integers are available to pop. If not,
                            // EmptyStackException is thrown.
                            a = stack.pop();
                            b = stack.pop();
                            stack.push(a * b);
                            break;
                        case 3:
                            // Pops two numbers, checks if either one is zero.
                            // If both are non-zero numbers, performs integer
                            // divison and pushes the result to the stack.
                            // Throws EmptyStackException if values cannot be
                            // found on the stack.
                            a = stack.pop();
                            b = stack.pop();
                            if(a == 0 || b == 0)
                                valid = false;
                            else
                                stack.push(b / a);
                            break;
                        case 4:
                            // Pops two numbers, checks if either one is zero.
                            // If both are non-zero numbers, performs integer
                            // mod and pushes the result to the stack.
                            // Throws EmptyStackException if values cannot be
                            // found on the stack.
                            a = stack.pop();
                            b = stack.pop();
                            if(a == 0 || b == 0)
                                valid = false;
                            else
                                stack.push(b % a);
                            break;
                        case 5:
                            // Pushes next number in the numbers ArrayList and
                            // then uses a ListIterator to delete it so that it
                            // is not processed in the next pass. This will add
                            // that value to the stack, and subsequently not
                            // process it within the program. If a value is not
                            // a number, NumberFormatException is thrown. If
                            // no value is available to push,
                            // EmptyStackException is thrown.
                            if(iterator.hasNext()){
                                try{
                                    stack.push(Integer.parseInt(iterator.next()));
                                }
                                catch(NumberFormatException e){
                                    valid = false;
                                }
                                iterator.remove();
                            }
                            else
                                valid = false;
                            break;
                        case 6:
                            // Pops two numbers, performs an arithmetic AND on
                            // then pushes the result to the stack. Throws
                            // EmptyStackException if values cannot be found on
                            // stack.
                            a = stack.pop();
                            b = stack.pop();
                            stack.push(a & b);
                            break;
                        case 7:
                            // Pops two numbers, performs an arithmetic OR on
                            // then pushes the result to the stack. Throws
                            // EmptyStackException if values cannot be found on
                            // stack.
                            a = stack.pop();
                            b = stack.pop();
                            stack.push(a | b);
                            break;
                        case 8:
                            // Pops one number, squares it, and then pushes it
                            // to the stack. Throws EmptyStackException if value
                            // cannot be found on stack.
                            a = stack.pop();
                            stack.push(a * a);
                            break;
                        case 9:
                            // Pops one number, tosses it. Throws
                            // EmptyStackException if value cannot be found on
                            // stack.
                            stack.pop();
                            break;
                        default:
                            break;
                    }
                } catch(EmptyStackException e){
                    // Tests validity of previous switch statements.
                    valid = false;
                }
            } catch(NumberFormatException e){
                // Tests if value passed is a valid integer value.
                valid = false;
            }
        }
        try{
            if(stack.pop() != 1)
            // The line, or program, is only valid if the top-most value of the
            // stack is 1, this tests if that is true.
                valid = false;
        } catch(EmptyStackException e){
            valid = false;
        }
        return(valid);
    }

    public static void main(String[] args){
        // Creates an instance of the parser, reads source.data, intreprets each
        // line of the line and validates each line. Clears the ArrayList and
        // stack following the processing of each line.
        microParse parser = new microParse();
        Boolean isGood = false;
        int l = 1;
        try{
            parser.read("source.data");
        }
        catch(FileNotFoundException e){
            System.out.println("No File Found");
        }

        for(String line : inputFile){
            for(String number : line.split("_"))
                numbers.add(number);
            isGood = parser.parse();
            if(isGood == true)
                System.out.println("Line " + l + " is valid");
            else
                System.out.println("Line " + l + " is invalid");
            numbers.clear();
            stack.clear();
            l++;
        }
    }
}
