
require("rooms/room1");
require("rooms/room2");
require("rooms/room3");
require("rooms/room4");
require("rooms/room5");
require("rooms/room6");
require("rooms/room7");
require("rooms/room8");
require("rooms/room9");
require("rooms/room10");
require("rooms/room11");
require("rooms/room12");
require("rooms/room13");
require("rooms/room14");
require("rooms/room15");
require("rooms/room16");
require("rooms/room17");
require("rooms/room18");
require("rooms/room19");
require("rooms/room20");
require("rooms/room21");
require("rooms/room22");
require("rooms/room23");
require("rooms/room24");
require("rooms/room25");
require("rooms/room26");
require("rooms/room27");
require("rooms/room28");
require("rooms/room29");
require("rooms/room30");
require("rooms/room31");
require("rooms/room32");
require("rooms/room33");
require("rooms/room34");
require("rooms/room35");
require("rooms/room36");
require("rooms/room37");
require("rooms/room38");
require("rooms/room39");
require("rooms/room40");
require("rooms/room41");
require("rooms/room42");

roomMap = {
	[1] = room1,
	[2] = room2,
	[3] = room3,
	[4] = room4,
	[5] = room5,
	[6] = room6,
	[7] = room7,
	[8] = room8,
	[9] = room9,
	[10] = room10,
	[11] = room11,
	[12] = room12,
	[13] = room13,
	[14] = room14,
	[15] = room15,
	[16] = room16,
	[17] = room17,
	[18] = room18,
	[19] = room19,
	[20] = room20,
	[21] = room21,
	[22] = room22,
	[23] = room23,
	[24] = room24,
	[25] = room25,
	[26] = room26,
	[27] = room27,
	[28] = room28,
	[29] = room29,
	[30] = room30,
	[31] = room31,
	[32] = room32,
	[33] = room33,
	[34] = room34,
	[35] = room35,
	[36] = room36,
	[37] = room37,
	[38] = room38,
	[39] = room39,
	[40] = room40,
	[41] = room41,
	[42] = room42,
	};