


function Flashlightinit()
	local myNumber = nextNumber;
	nextNumber = nextNumber + 1;

	mapping["flashlight"] = {[0] = myNumber, [1] = 0};
	mapping["Flashlight"] = {[0] = myNumber, [1] = 0};
	mapping["light"] = {[0] = myNumber, [1] = 0};
	mapping["Light"] = {[0] = myNumber, [1] = 0};

	items[myNumber] = {[0] = "flashlight", [1] = 30, [2] = 0, [3] = "a", [4] = useFlashlight};


end


function useFlashlight(arg)
	local mynum = mapping["flashlight"][0];
	local myStuff = items[mynum];


	if (arg == 0) then
		print ("  "..myStuff[3].." "..myStuff[0]);
	end

	if ((arg == 12)and(myStuff[1]==location))  then
		print ("You acquire "..myStuff[3].." "..myStuff[0]..".");
		myStuff[1] = 0;
	end

	if ((arg == 13)and(myStuff[1]==0))  then
		print (myStuff[0].." dropped.");
		myStuff[1] = location;
	end

	if ((arg == 14)and(myStuff[1]==0)) then
		if(lightOn == 1) then
			print("The flashlight goes dark.");
			lightOn = 0;
		else
			print("The flashlight shines brightly.");
			lightOn = 1;
		end
	end

	if ((arg == 17)and(myStuff[1]==0)) then
		print("It's a rugged old flashlight.");
	end

end
