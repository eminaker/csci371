-- Eric Minaker
-- CSCI 371
-- Lua Text Game
-- Spring 2016
-- In order to win the game, you must obtain two kilos of candy from Kyler.
-- This can be achieved by purchashing them for 500 funds each, the instructions
-- for gathering the funds, and associated rules, follow.
--
-- In order to obtain the first 500 funds:
-- North
-- North
-- West
-- West
-- Take light
-- Use light
-- 		The light will last 11 turns and must be used prior to entering the
-- 		tunnels. You may toggle the light on and off, but you are only allowed
-- 		3 turns in the tunnels without the light before you trigger an endgame. 
-- 		The light will, as long as the power is turned on, auto-toggle when
--  	entering and exiting dark areas. If you manually toggle the power, the
--  	light will not be able to auto-toggle. The light sensor requires power!
-- North
-- East
-- North
-- East
-- South
-- East
-- South
-- Take book
-- South
-- West
-- West
-- North
-- North
-- North
-- East
--
-- This will place you in the CAS Lobby, from here you may go south and buy a
-- piece of candy, the following instructions will assume you are in the CAS
-- Lobby.
--
-- To get the second 500:
-- East
-- North
-- North
-- Take key
-- South
-- South
-- West
-- West
-- South
-- South
-- South
-- East
-- East
-- South
-- West
-- South
-- 		This movement will require the key, should you drop the key it will not
-- 		work properly. This is not a destructive lock though, you keep the key
-- 		until you choose to drop it and can access this door at any point with
-- 		the key.
-- Take car
-- Take stash
-- North
-- North
-- North
-- East
-- South
-- Buy candy from Kyler
-- 		If you chose to do both adventures in sequence, execute this twice to
-- 		win.

require("parse")
require("superparser");
require("verbs");
require("support")

require("rooms/roomsetup");


require("people/kyler");
require("people/sean");
require("people/gerberding");
require("people/minaker");
require("people/postma");
require("people/cat");
require("people/schemmbot");

require("things/map");
require("things/usb");
require("things/atm");
require("things/atmcard");
require("things/candy");
require("things/key");
require("things/flashlight");
require("things/yogabook");
require("things/seanstash");
require("things/seancar");

require("verbs/talk");
require("verbs/buy");
require("verbs/get");
require("verbs/drop");
require("verbs/inventory");
require("verbs/look");
require("verbs/attack");
require("verbs/give");
require("verbs/use");

funds = 50;
lastVerb=nil;
karma = 1;
banned= 0;
candy =0;
lightOn = 0;
lightTick = 0;
inTunnels = 0;
grueTick = 0;

-- -------------------------------------------------------
-- Initialize the Tables for mappings, items, and people


-- Mapping consists of a ID and a type [0 = item, 1 = person]
mapping = { };

--[[ Items are indexed by ID, and consist of a formal name[0],
																					a location[1] (-1, 0, or room)
																					a status[2]
																					an article [3]
																					a use function[4]
--]]
items = { };

--[[ People are indexed by ID, and conists of a formal name[0],
																				   a location[1],
																					 a status[2],
																					 an interact function[3]
																					 a tick function[4]
--]]
people = { };
nextNumber = 0;

-- --------------------------------------------------------


Kylerinit();
Seaninit();
Gerberdinginit();
Minakerinit();
Postmainit();
Catinit();
Schemmbotinit();
Seanstashinit();
Seancarinit();



Mapinit();
USBinit();
Atminit();
AtmCardinit();
Candyinit();
Keyinit();
Flashlightinit();
Yogabookinit();

-- -------------------------------------------------------------
-- Start main loop
--
-- New for Spring 16 - Added a tick to the main loop
--
-- ------------------------------------------------------------
location = 1
tickCount = 0;
while ( true ) do
	location = roomMap[location]()
	tickCount = tickCount + 1;
	tick()
end
