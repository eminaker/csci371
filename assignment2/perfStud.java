/*
 * Eric Minaker
 * CSCI371
 * Assignment 2
 * The following will read in three files, named boynames, girlnames, and
 * lastnames. It will select 10 boys names, with unique first letters, 10 girls
 * names with unique first letters and 20 last names with unique first letters.
 * It will then generate 20 GPAs between 3.20 - 4.00 with no repeat values.
 * The names will be concatonated and assigned a GPA and printed to a file named
 * PerfectClass. The GPA will be printed to the second decimal place.
 */
import java.util.*;
import java.io.*;
import java.text.*;

public class perfStud{
    public static ArrayList<String> boys = new ArrayList<String>();
    public static ArrayList<String> girls = new ArrayList<String>();
    public static ArrayList<String> middle = new ArrayList<String>();
    public static ArrayList<String> last = new ArrayList<String>();
    public static HashMap<String, String> lastChose =
                                                new HashMap<String, String>();
    public static ArrayList<Double> gpa = new ArrayList<Double>();
    public static HashMap<String, String> boyFirst =
                                                new HashMap<String, String>();
    public static HashMap<String, String> girlFirst =
                                                new HashMap<String, String>();
    public static Random rand = new Random();

    public void readNames(String filename, ArrayList<String> nameType)
                          throws FileNotFoundException{
        // Read the names in a given file and add them to an ArrayList for
        // future use.
        Scanner fileIn = new Scanner(new FileReader(filename));
        while(fileIn.hasNextLine()){
            String line = fileIn.nextLine();
            nameType.add(line);
        }
    }

    public void populateGpa(){
        // Create 20 random numbers from 3.20 - 4.00, each being a unique value
        // and put them into an ArrayList for future use.
        double result = 0;
        for(int i = 0; i < 20; i++){
            do{
                result = rand.nextDouble() * (4.00 - 3.20) + 3.20;
                result = Math.round(result * 100);
                result = result/100;
            }while(gpa.contains(result));
            gpa.add(result);
        }
    }

    public void populateMiddle(){
        // Create an ArrayList of 20 random letters with no repeating letters.
        String result = new String();
        int temp = 0;
        for(int i = 0; i < 20; i++){
            do{
                temp = rand.nextInt(90 - 65 + 1)+ 65;
                result = Character.toString((char)temp);
            }while(middle.contains(result));
            middle.add(result);
        }
    }

    public void populateLast(){
        // Using the ArrayList last (populated using readNames and the lastnames
        // file), select 20 random names with unique first letters and place
        // them into a HashMap where the first letter is the key and the entire
        // name is the value.
        String result = new String();
        int temp = 0;
        for(int i = 0; i < 20; i++){
            do{
                temp = 1 + rand.nextInt(last.size() - 2 + 1);
                result = last.get(temp);
                lastChose.put(Character.toString(result.charAt(0)), result);
            }while(lastChose.size() < 20);
        }
    }

    public void populateName(ArrayList<String> type,
                             HashMap<String, String> dest){
        // Given an ArrayList and HashMap (ideally an ArrayList containing names
        // from the files boynames and girlnames accompanied by an empty
        // HashMap), select 20 random names and put them into the HashMap using
        // the first letter of the name as the key and the entire name as the
        // value.
        String result = new String();
        int temp = 0;
        for(int i = 0; i < 26; i++){
            do{
                temp = 1 + rand.nextInt(type.size() - 2 + 1);
                result = type.get(temp);
                dest.put(Character.toString(result.charAt(0)), result);
            }while(dest.size() < 20);
        }
    }

    public void makeStudent(){
        // Using the boyFirst, girlFirst, and lastChose HashMaps along with the
        // middle and gpa ArrayList, outputs 20 students, consisting of a first
        // name, middle initial, last name, and a GPA. All will have first and
        // last names with unique first letters and no repeating GPAs or middle
        // initials. The first letter of the last and first names may not be
        // unique. A student with the name Dwight D. Donaldson is possible, for
        // example. The GPA will always be printed to two decimal places.
        DecimalFormat df = new DecimalFormat();
        try{
            PrintWriter p = new PrintWriter("PerfectClass", "UTF-8");
            df.setMinimumFractionDigits(2);
            //ArrayList<String> lastArray = new ArrayList<String>();
            //lastArray = Arrays.asList(lastChose.values().toArray());
            List<String> lastArray = Arrays.asList(
                                     lastChose.values().toArray(new String[0]));
            Collections.reverse(lastArray);
            for(int i = 0; i < 20; i++){
                if(i < 10){
                    p.println(boyFirst.values().toArray()[i] +
                                        " " + middle.get(i) + ". " +
                                        lastArray.get(i) + " " +
                                        df.format(gpa.get(i)));
                }
                else{
                    p.println(girlFirst.values().toArray()[i] +
                                        " " + middle.get(i) + ". " +
                                        lastArray.get(i) + " " +
                                        df.format(gpa.get(i)));
                }
            }
            p.close();
        }catch(UnsupportedEncodingException e){
            System.out.println("Unsupported Encoding Exception");
        }catch(FileNotFoundException e){
            System.out.println("File not found");
        }
    }

    public static void main(String[] args){
        // Call the approriate functions to read in the name files, populate
        // random GPAs and middle initials, select 20 boy, girl, and last names
        // that fit the criteria identified at the beginning of the program, and
        // print 20 students to a file named PerfectClass.
        perfStud generator = new perfStud();

        try{
            generator.readNames("boynames", boys);
            generator.readNames("girlnames", girls);
            generator.readNames("lastnames", last);
        }catch(FileNotFoundException e){
            System.out.println("File not found");
        }

        generator.populateGpa();
        generator.populateMiddle();
        generator.populateLast();
        generator.populateName(boys, boyFirst);
        generator.populateName(girls, girlFirst);

        generator.makeStudent();
    }
}
