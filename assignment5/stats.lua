#! /usr/bin/lua
-- Eric Minaker
-- Assignment 5
-- Stats
-- The following program will read a data file, people.data, and fill a two-
-- dimensional table with the names of brave adventurers and a series of
-- arbitrary stats in the following format:
--      name stat 0
-- It will then read a file called adventure.data and check the names and
-- stats contained against the table. The file should be setup as follows:
--      name stat
-- It will print the name of the adventurer,the stat, and, if the stat was 
-- added in the people.data file, the value for that stat. If the stat was not 
-- defined previously, a 0 will be printed in place of the value. If the 
-- adventurer was not defined previously, the name and stat of the requested 
-- adventurer will be printed with a 0 for the value.
list = {}
n=1
for line in io.lines('people.data') do
    name, stat, num = string.match(line, "(.*) (.*) (.*)")
    if(list[name] ~= nil) then
        list[name][stat]=num
    else
        list[name] = {[stat]=num}
    end
end
for line in io.lines('adventure.data') do
    name, stat = string.match(line, "(.*) (.*)")
    print("------------------------------")
    if(list[name] ~= nil and list[name][stat] ~= nil) then
        print("Line "..n..": "..name.." "..stat.." "..list[name][stat])
    else
        print("Line "..n..": "..name.." "..stat.." 0")
    end
    print("------------------------------")
    n=n+1
end
