#! /usr/bin/perl

# CSCI371
# Assignment 3
# Eric Minaker
#
# The following uses three given files, boynames, girlnames, and lastnames
# and chooses 20 random names from each. It will also generate 20 random
# middle initials and 20 random GPAs between 3.20 and 4.00. The first and
# last names are assured to not have any repeating first letters. The middle
# initials and GPAs are always assured to not have any repeats. This list
# of names and GPAs, ten boys and ten girls,  will be printed to a file named 
# PerfectClass with the following format:
# First M. Last 3.55

use warnings;
use autodie;

use Path::Class;

# Files and file handlers

my $boyFile = file('boynames');
my $girlFile = file('girlnames');
my $lastFile = file('lastnames');

my $boyHandle = $boyFile->openr();
my $girlHandle = $girlFile->openr();
my $lastHandle = $lastFile->openr();

# Arrays and hashes for later use in gathering, selecting, and sorting names
# and GPAs.

my @boyNames;
my @girlNames;
my @lastNames;
my @middleInitials = (A..Z);
my @gpa = (320..400);

my %boySelect;
my %girlSelect;
my %lastSelect;
my %middleSelect;
my %gpaSelect;

# Read the three files and place the names into arrays

while(my $line = $boyHandle->getline()) {
    push(@boyNames, $line);
}

while(my $line = $girlHandle->getline()){
    push(@girlNames, $line);
}

while(my $line = $lastHandle->getline()){
    push(@lastNames, $line);
}

# Choose names, initials, and GPAs at random from the previously constructed
# arrays and place them into hashes. These hashes will use the first letter
# of the name as the key and the entire name as the value. For the middle
# initials, the initial will be both the key and the value. For GPAs, the
# key will be the GPA multiplied by 100 and the value will be the GPA itself.

my $boyHashSize = 0;
my $girlHashSize = 0;
my $gpaHashSize = 0;
my $lastHashSize = 0;
my $middleHashSize = 0;

while($boyHashSize < 22){
    my $arrSize = @boyNames;
    my $randSel = $boyNames[int(rand($arrSize))];
    $boySelect{substr($randSel, 0 , 1)} = $randSel;
    $boyHashSize = keys %boySelect;
}

while($girlHashSize < 22){
    my $arrSize = @girlNames;
    my $randSel = $girlNames[int(rand($arrSize))];
    $girlSelect{substr($randSel, 0 , 1)} = $randSel;
    $girlHashSize = keys %girlSelect;
}

while($lastHashSize < 20){
    my $arrSize = @lastNames;
    my $randSel = $lastNames[int(rand($arrSize))];
    $lastSelect{substr($randSel, 0 , 1)} = $randSel;
    $lastHashSize = keys %lastSelect;
}

while($middleHashSize < 20){
    my $arrSize = @middleInitials;
    my $randSel = $middleInitials[int(rand($arrSize))];
    $middleSelect{substr($randSel, 0 , 1)} = $randSel;
    $middleHashSize = keys %middleSelect;
}

while($gpaHashSize < 20){
    my $arrSize = @gpa;
    my $randSel = $gpa[int(rand($arrSize))];
    $gpaSelect{$randSel} = $randSel/100;
    $gpaHashSize = keys %gpaSelect;
}

# Format and print the names to a file named PerfectClass located in the
# current local directory.

my @boyOut = values %boySelect;
my @girlOut = values %girlSelect;
my @lastOut = values %lastSelect;
my @middleOut = values %middleSelect;
my @gpaOut = values %gpaSelect;
@boyOut = sort @boyOut;
@girlOut = sort @girlOut;

open(my $fh, '>', 'PerfectClass');

for(my $i=0; $i<10; $i++){
    $gpaOutF = sprintf("%.2f", $gpaOut[$i]);
    chomp($boyOut[$i]);
    chomp($lastOut[$i]);
    print $fh "$boyOut[$i] $middleOut[$i]. $lastOut[$i] $gpaOutF\n";
}

for(my $i=0; $i<10; $i++){
    $gpaOutF = sprintf("%.2f", $gpaOut[$i+10]);
    chomp($girlOut[$i+11]);
    chomp($lastOut[$i+10]);
    print $fh "$girlOut[$i+11] $middleOut[$i+10]. $lastOut[$i+10] $gpaOutF\n";
}

close $fh;
