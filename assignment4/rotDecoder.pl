#! /usr/bin/perl

# Eric Minaker
# CSCI 371
# Assignment 4
# 03/13/16
#
# The following will read an arbitrary number of files given at the command
# line and decode each one, line by line. It will run each line through all 25
# ROT encodings, printing out only the results which contain "the" somewhere
# in the decoded text. It will maintain the original case of the given
# strings and ignore any numbers or puncuation.
#
# You should invoke the program as follows:
# ./rotDecoder.pl FILE [OPTIONAL FILES]

foreach $line (<>){
    foreach $i (1..25){
        @line = split / /, $line;
        foreach $word (@line){
            @word = split //, $word;
            foreach $letter (@word){
                # If the letter is lowercase
                if(ord($letter) >= 97 && ord($letter) <= 122){
                    # Cast the letter to an int as its ASCII value, subtract
                    # the minimum value, add the offset, modulo by the range,
                    # add the minimum value, cast to character.
                    $asciiDC = chr((((ord($letter) - 97) + $i) % 26) + 97);
                    push(@wordTranslate, $asciiDC);
                }
                # If the letter is uppercase
                elsif(ord($letter) >= 65 && ord($letter) <= 90){
                    $asciiDC = chr((((ord($letter) - 65) + $i) % 26) + 65);
                    push(@wordTranslate, $asciiDC);
                }
                # If it's not a letter
                else{
                    push(@wordTranslate, $letter);
                }
            }
            push(@lineTranslate, join("",@wordTranslate));
            @wordTranslate = ();
        }
        $scal = join(" ",@lineTranslate);
        if(index(lc($scal), "the") != -1){
            print $scal;
        }
        @lineTranslate = ();
    }
}
